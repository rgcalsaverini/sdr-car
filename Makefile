OBJS = src/Interface.cpp src/main.cpp src/CarController.cpp
OBJS_OLD = src/ook.c
COMPILER_FLAGS = -Wall -std=c++17 -g
OBJ_NAME = bin/car

debug : $(OBJS)
	g++ $(OBJS) -w -o $(OBJ_NAME) $(COMPILER_FLAGS) `pkg-config --cflags --libs sdl2` -lSDL2_image -lSDL2_ttf -lhackrf

old: $(OBJS_OLD)
	g++ $(OBJS_OLD) -w -o $(OBJ_NAME)_old $(COMPILER_FLAGS) `pkg-config --cflags --libs sdl` -lSDL_image -lhackrf

.POHNY: valgrind
valgrind: debug
	valgrind --tool=memcheck --leak-check=full --track-origins=yes  --num-callers=20 --track-fds=yes --log-file=valg.out --suppressions=minimal.supp ./bin/car

.POHNY: gen_supp
gen_supp: debug
	valgrind --tool=memcheck --leak-check=yes --track-origins=yes --show-reachable=yes --num-callers=20 --track-fds=yes --gen-suppressions=all --log-file=valg.out --error-limit=no ./bin/car
	cat ./valg.out | ./parse_valgrind_suppressions.sh > minimal.supp
	rm valg.out

.POHNY: clean
clean:
	rm bin/ook
	rmdir bin
