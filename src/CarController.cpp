#include "CarController.h"
#include <stdexcept>
#include <cmath>
#include <iostream>
#include <unistd.h>

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

map<size_t, CarController::SignalU8> messages;
CarController::Directions direction = CarController::STOP;

CarController::CarController(unsigned int grid_us, uint64_t base_frequency, uint32_t sample_rate, uint32_t gain) :
        sample_rate(sample_rate), gain(gain), base_frequency(base_frequency), grid_us(grid_us) {
    amp = true;
    msg_descriptors[Directions::FWD] = 10;
    msg_descriptors[Directions::BACK] = 40;
    msg_descriptors[Directions::RIGHT] = 64;
    msg_descriptors[Directions::LEFT] = 58;
    msg_descriptors[Directions::FWDRIGHT] = 34;
    msg_descriptors[Directions::FWDLEFT] = 28;
    msg_descriptors[Directions::BACKRIGHT] = 46;
    msg_descriptors[Directions::BACKLEFT] = 52;


    key_on = keyOnFor(grid_us);
    key_off = keyOffFor(grid_us);

    for (size_t direction = 0; direction < Directions::STOP; direction++) {
        messages[direction] = encodeMessage(msg_descriptors[direction]);
    }
}

CarController::~CarController() {
    if (device != nullptr && device != NULL) {
        hackrf_close(device);
        hackrf_exit();
    }
    free(key_on.first);
    free(key_off.first);
    for (size_t direction = 0; direction < Directions::STOP; direction++) {
        free(messages[direction].first);
    }
}

void CarController::startTX(size_t new_direction) {
    std::cout << "START\n";
    direction = (Directions) new_direction;
    hackrf_start_tx(device, TXCallback, nullptr);
}

void CarController::stopTX() {
    std::cout << "STOP\n";
    hackrf_stop_tx(device);
}

int CarController::TXCallback(hackrf_transfer *transfer) {
//    std::cout << direction << std::endl;
    if (direction != STOP) {
        auto bytes_to_copy = (size_t) transfer->valid_length;
        size_t bytes_copied = 0;
        while (bytes_copied < bytes_to_copy) {
            auto msg_copy_len = messages[direction].second;
            if (msg_copy_len + messages[direction].second > bytes_to_copy) {
                msg_copy_len = bytes_to_copy - bytes_copied;
            }
            memcpy(transfer->buffer + bytes_copied, messages[direction].first, msg_copy_len);
            bytes_copied += msg_copy_len;
        }
    }
    usleep(10);
}

bool CarController::setupHackRF(void(Interface::*onReady)(std::string, unsigned long, unsigned long), Interface *obj) {
    int result = HACKRF_TRUE;
    uint32_t bwf_freq;

    bwf_freq = hackrf_compute_baseband_filter_bw_round_down_lt(sample_rate);

    result = hackrf_init();

    result =  hackrf_open(&device)
             || hackrf_set_sample_rate_manual(device, sample_rate, 1)
             || hackrf_set_baseband_filter_bandwidth(device, bwf_freq)
             || hackrf_set_txvga_gain(device, gain)
             || hackrf_set_freq(device, base_frequency)
             || hackrf_set_amp_enable(device, (uint8_t) amp);

    if (result != HACKRF_SUCCESS && device != nullptr) {
        device = nullptr;
    }

    if (onReady != nullptr) {
        if (result != HACKRF_SUCCESS) {
            (*obj.*onReady)("", 0, 0);
        } else {
            hackrf_board_partid_serialno_read(device, &part_serial_number);
            char serial[20];
            sprintf(serial, "%08x", part_serial_number.part_id[1]);
            (*obj.*onReady)(serial, base_frequency, sample_rate);
        }
    }

    return result == HACKRF_SUCCESS;
}


CarController::SignalU8 CarController::keyOnFor(unsigned int duration_us) {
    SignalU8 signal;
    auto sample_size = (uint64_t) (duration_us * (sample_rate / 1000000.0));
    double freq_quo = (2.0 * M_PI) / (double) (sample_size);
    signal.first = (signal_t *) malloc(sample_size);
    signal.second = sample_size;

    for (unsigned long x = 0; x < sample_size; x++) {
        signal.first[x] = (signal_t) (250 * sin(freq_quo * x));
    }

    return signal;
}

CarController::SignalU8 CarController::keyOffFor(unsigned int duration_us) {
    auto samples = (uint64_t) (duration_us * (sample_rate / 1000000.0));
    SignalU8 signal;
    signal.first = (signal_t *) calloc(samples, sizeof(signal_t));
    signal.second = samples;
    return signal;
}

CarController::SignalU8 CarController::encodeMessage(unsigned int short_pulses) {
    SignalU8 encoded;
    unsigned long msg_length = (3 + short_pulses) * key_on.second + (1 + short_pulses) * key_off.second;
    encoded.second = msg_length;
    encoded.first = (signal_t *) malloc(msg_length);
    unsigned long idx = 0;

    for (unsigned int i = 0; i < 3; i++) {
        memcpy(encoded.first + idx, key_on.first, key_on.second);
        idx += key_on.second;
    }
    memcpy(encoded.first + idx, key_off.first, key_off.second);
    idx += key_off.second;

    for (unsigned int i = 0; i < short_pulses; i++) {
        memcpy(encoded.first + idx, key_on.first, key_on.second);
        idx += key_on.second;
        memcpy(encoded.first + idx, key_off.first, key_off.second);
        idx += key_off.second;
    }

    return encoded;
}

