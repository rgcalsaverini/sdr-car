#include <libhackrf/hackrf.h>
#include <stdio.h>
#include <signal.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include "ook.h"
#include <math.h>
#include "SDL/SDL.h"
#include "SDL/SDL_image.h"

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

short int keep_running = true;

int active_msg = STOP;
rf_signal tx_messages[8];
unsigned long msg_pos = 0;

void killsig_handler(int signum) {
    keep_running = false;
}

void exit_hackrf_error(enum hackrf_error result) {
    fprintf(stderr,
            "Aborting with HackRF error: %s (%d)\n", hackrf_error_name(result), result);
    exit(EXIT_FAILURE);
}

void setup_device(hackrf_device **device, ook_configs configs) {
    int result;
    uint32_t bwf_freq;

    bwf_freq = hackrf_compute_baseband_filter_bw_round_down_lt(configs.sample_rate);

    result = hackrf_init()
             || hackrf_open(device)
             || hackrf_set_sample_rate_manual(*device, configs.sample_rate, 1)
             || hackrf_set_baseband_filter_bandwidth(*device, bwf_freq)
             || hackrf_set_txvga_gain(*device, configs.gain)
             || hackrf_set_freq(*device, configs.base_frequency)
             || hackrf_set_amp_enable(*device, configs.amp);

    if (result != HACKRF_SUCCESS) {
        exit_hackrf_error((enum hackrf_error) result);
    }
}

int tx_callback(hackrf_transfer *transfer) {
    size_t bytes_to_read = (size_t) transfer->valid_length;
    size_t bytes_read = 0;
    while (bytes_read < bytes_to_read) {
        transfer->buffer[bytes_read] = (uint8_t) (tx_messages[active_msg].signal[msg_pos] % 0xFF);
        transfer->buffer[bytes_read + 1] = (uint8_t) (tx_messages[active_msg].signal[msg_pos] / 0xFF);;
        msg_pos = (msg_pos + 1) % tx_messages[active_msg].length;
        bytes_read += 2;
    }
    usleep(10);
}

rf_signal key_on_for(unsigned int duration_us, uint32_t sample_rate) {
    rf_signal signal;
    signal.length = (size_t) (duration_us * (sample_rate / 1000000.0));
    signal.signal = (signal_pt *) malloc(sizeof(signal_pt) * signal.length);

    double ma = 2.0 * M_PI * offset_freq;
    double db_sr = (double) (sample_rate);

    for (unsigned long x = 0; x < signal.length; x++) {
        signal.signal[x] = (signal_pt) (max_amplitude * sin(ma * x / db_sr));
    }
    return signal;
}

rf_signal key_off_for(unsigned int duration_us, uint32_t sample_rate) {
    rf_signal signal;
    signal.length = (unsigned long) (duration_us * (sample_rate / 1000000.0));
    signal.signal = (signal_pt *) calloc(signal.length, sizeof(signal_pt));
    return signal;
}

unsigned long message_duration_us(unsigned int *message, unsigned long length, unsigned long on, unsigned long off) {
    unsigned long sum = 0;
    for (unsigned long i = 0; i < length; i++) {
        sum += off + message[i] * on;
    }
    return sum;
}

rf_signal encode_message(msg_descr message, rf_signal on, rf_signal off) {
    rf_signal enc_msg;
    enc_msg.length = message_duration_us(message.message, message.length, on.length, off.length);
    enc_msg.signal = (signal_pt *) malloc(enc_msg.length * sizeof(signal_pt));
    size_t idx = 0;
    size_t on_size = on.length;
    size_t off_size = off.length;

    for (unsigned int seg = 0; seg < message.length; seg++) {
        for (unsigned int pulse = 0; pulse < message.message[seg]; pulse++) {
            memcpy(enc_msg.signal + idx, on.signal, on_size * sizeof(signal_pt));
            idx += on_size;
        }
        memcpy(enc_msg.signal + idx, off.signal, off_size * sizeof(signal_pt));
        idx += off_size;
    }

    return enc_msg;
}

rf_signal repeat_encoded_message(rf_signal message, unsigned long duration_us, uint32_t sample_rate) {
    uint32_t pts_on_us = sample_rate / 1000000;
    unsigned long msg_duration_us = message.length / pts_on_us;
    unsigned long repetitions = duration_us / msg_duration_us;
    rf_signal full_message;
    full_message.length = repetitions * message.length;
    full_message.signal = (signal_pt *) malloc(full_message.length * sizeof(signal_pt));
    for (unsigned long rep = 0; rep < repetitions; rep++) {
        memcpy(full_message.signal + (message.length * rep), message.signal, message.length * sizeof(signal_pt));
    }
    return full_message;
}


msg_descr generate_msg_descriptor(unsigned int long_pulses, unsigned int short_pulses) {
    msg_descr descriptor;
    descriptor.length = long_pulses + short_pulses;
    descriptor.message = (unsigned int*) malloc(descriptor.length * sizeof(unsigned int));
    for (unsigned int i = 0; i < long_pulses; i++) {
        descriptor.message[i] = 3;
    }
    for (unsigned int i = 0; i < short_pulses; i++) {
        descriptor.message[i + long_pulses] = 1;
    }
    return descriptor;
}

int main(int argc, char **argv) {
    static hackrf_device *device = NULL;
    ook_configs configs;

    signal(SIGINT, &killsig_handler);
    signal(SIGILL, &killsig_handler);
    signal(SIGFPE, &killsig_handler);
    signal(SIGSEGV, &killsig_handler);
    signal(SIGTERM, &killsig_handler);
    signal(SIGABRT, &killsig_handler);

    configs.sample_rate = 4000000;
    configs.base_frequency = 27231000;
    configs.gain = 47;
    configs.amp = ENABLED;
    configs.grid_us = 384;

    msg_descr descriptors[8];

    descriptors[FWD] = generate_msg_descriptor(4, 10);
    descriptors[BACK] = generate_msg_descriptor(4, 40);
    descriptors[LEFT] = generate_msg_descriptor(4, 58);
    descriptors[RIGHT] = generate_msg_descriptor(4, 64);
    descriptors[FWDRIGH] = generate_msg_descriptor(4, 34);
    descriptors[FWDLEFT] = generate_msg_descriptor(4, 28);
    descriptors[BACKRIGHT] = generate_msg_descriptor(4, 46);
    descriptors[BACKLEFT] = generate_msg_descriptor(4, 52);

    rf_signal on_signal = key_on_for(configs.grid_us, configs.sample_rate);
    rf_signal off_signal = key_off_for(configs.grid_us, configs.sample_rate);
    tx_messages[FWD] = encode_message(descriptors[FWD], on_signal, off_signal);
    tx_messages[BACK] = encode_message(descriptors[BACK], on_signal, off_signal);
    tx_messages[LEFT] = encode_message(descriptors[LEFT], on_signal, off_signal);
    tx_messages[RIGHT] = encode_message(descriptors[RIGHT], on_signal, off_signal);
    tx_messages[FWDRIGH] = encode_message(descriptors[FWDRIGH], on_signal, off_signal);
    tx_messages[FWDLEFT] = encode_message(descriptors[FWDLEFT], on_signal, off_signal);
    tx_messages[BACKRIGHT] = encode_message(descriptors[BACKRIGHT], on_signal, off_signal);
    tx_messages[BACKLEFT] = encode_message(descriptors[BACKLEFT], on_signal, off_signal);
    tx_messages[STOP] = encode_message(generate_msg_descriptor(4, 0), on_signal, off_signal);

    SDL_Init(SDL_INIT_EVERYTHING);
    SDL_WM_SetCaption("SDL Test", "SDL Test");
    IMG_Init(IMG_INIT_JPG | IMG_INIT_PNG);
    SDL_Surface *screen = SDL_SetVideoMode(496, 496, 32, SDL_SWSURFACE | SDL_NOFRAME);
    SDL_Surface *background = IMG_Load("./images/background.png");
    SDL_Surface *arrow_t = IMG_Load("./images/t.png");
    SDL_Surface *arrow_b = IMG_Load("./images/b.png");
    SDL_Surface *arrow_l = IMG_Load("./images/l.png");
    SDL_Surface *arrow_r = IMG_Load("./images/r.png");
    SDL_Event event;

    short key_w_down = false;
    short key_a_down = false;
    short key_s_down = false;
    short key_d_down = false;
    short vertical;
    short horizontal;
    SDL_Rect sdl_rect;
    setup_device(&device, configs);

    while (keep_running && device != NULL && hackrf_is_streaming(device)) {
        if (SDL_PollEvent(&event)) {
            if (event.type == SDL_QUIT) {
                keep_running = false;
                break;
            } else if (event.type == SDL_KEYDOWN) {
                switch (event.key.keysym.sym) {
                    case SDLK_ESCAPE:
                        keep_running = false;
                        break;
                    case SDLK_w:
                        key_w_down = true;
                        break;
                    case SDLK_UP:
                        key_w_down = true;
                        break;
                    case SDLK_a:
                        key_a_down = true;
                        break;
                    case SDLK_LEFT:
                        key_a_down = true;
                        break;
                    case SDLK_s:
                        key_s_down = true;
                        break;
                    case SDLK_DOWN:
                        key_s_down = true;
                        break;
                    case SDLK_d:
                        key_d_down = true;
                        break;
                    case SDLK_RIGHT:
                        key_d_down = true;
                        break;

                }
            } else if (event.type == SDL_KEYUP) {
                switch (event.key.keysym.sym) {
                    case SDLK_ESCAPE:
                        keep_running = false;
                        break;
                    case SDLK_w:
                        key_w_down = false;
                        break;
                    case SDLK_UP:
                        key_w_down = false;
                        break;
                    case SDLK_a:
                        key_a_down = false;
                        break;
                    case SDLK_LEFT:
                        key_a_down = false;
                        break;
                    case SDLK_s:
                        key_s_down = false;
                        break;
                    case SDLK_DOWN:
                        key_s_down = false;
                        break;
                    case SDLK_d:
                        key_d_down = false;
                        break;
                    case SDLK_RIGHT:
                        key_d_down = false;
                        break;

                }
            }
        }
        vertical = key_w_down - key_s_down;
        horizontal = key_d_down - key_a_down;

        SDL_BlitSurface(background, NULL, screen, NULL);

        if(vertical == 0 && horizontal == 0) {
            hackrf_stop_tx(device);
        } else {
            hackrf_start_tx(device, tx_callback, NULL);
            if (vertical == 1) {
                if (horizontal == 1) active_msg = FWDRIGH;
                else if (horizontal == 0) active_msg = FWD;
                else if (horizontal == -1) active_msg = FWDLEFT;
            } else if (vertical == 0) {
                if (horizontal == 1) active_msg = RIGHT;
                else if (horizontal == -1) active_msg = LEFT;
            } else if (vertical == -1) {
                if (horizontal == 1) active_msg = BACKRIGHT;
                else if (horizontal == 0) active_msg = BACK;
                else if (horizontal == -1) active_msg = BACKLEFT;
            }
        }

        if (key_w_down) {
            sdl_rect.x = 212;
            sdl_rect.y = 98;
            sdl_rect.w = 71;
            sdl_rect.h = 47;
            SDL_BlitSurface(arrow_t, NULL, screen, &sdl_rect);
        }

        if (key_s_down) {
            sdl_rect.x = 212;
            sdl_rect.y = 352;
            sdl_rect.w = 71;
            sdl_rect.h = 47;
            SDL_BlitSurface(arrow_b, NULL, screen, &sdl_rect);
        }

        if (key_d_down) {
            sdl_rect.x = 308;
            sdl_rect.y = 212;
            sdl_rect.w = 47;
            sdl_rect.h = 71;
            SDL_BlitSurface(arrow_r, NULL, screen, &sdl_rect);
        }
        if (key_a_down) {
            sdl_rect.x = 140;
            sdl_rect.y = 212;
            sdl_rect.w = 47;
            sdl_rect.h = 71;
            SDL_BlitSurface(arrow_l, NULL, screen, &sdl_rect);
        }

        SDL_Flip(screen);
    }

    printf("\nStopping...\n");

    SDL_FreeSurface(background);
    SDL_Quit();

    if (device != NULL) {
        hackrf_stop_tx(device);
        hackrf_close(device);
    }
    hackrf_exit();
}
