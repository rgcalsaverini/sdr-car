//
// Created by rui on 22.04.18.
//

#ifndef C_COMM_INTERFACE_H
#define C_COMM_INTERFACE_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <string>
#include <vector>
#include <map>
//#include "CarController.h"

using std::vector;
using std::string;
using std::map;
using std::pair;

class CarController;

class Interface {
public:
    enum Sprites : size_t {
        BG, ARROW_T, ARROW_B, ARROW_R, ARROW_L, ICO_RADIO, CAR, TX_FALSE, TX_TRUE, SPRITES_LEN
    };
    enum Keys : int {
        UP, DOWN, LEFT, RIGHT,
    };

    using FullTexture = pair<SDL_Texture *, pair<int, int>>;
public:

    Interface();

    ~Interface();

    bool mainLoopStep(void(CarController::*startTX)(size_t), void(CarController::*stopTX)(), CarController *obj);

    void setDeviceId(std::string device = "");

    void setFrequency(unsigned long frequency = 0);

    void setSampleRate(unsigned long sr = 0);

    void setHackRf(std::string device, unsigned long frequency, unsigned long sr);

private:
    void renderSprite(size_t sprite, int x, int y);

    void renderCenteredText(FullTexture texture, int x, int y, int w);

    SDL_Texture *loadTexture(std::string path);

    FullTexture text2Texture(std::string text);

    string long2HzString(unsigned long);

    void calcDirection();

    void renderScreen();

    bool poolEvents();

private:
    map<int, int> capture_keys;
    map<int, bool> key_down;
    SDL_Window *window;
    SDL_Renderer *renderer;
    SDL_Surface *screen;
    SDL_Texture *sprites_img;
    vector<SDL_Rect> sprite_clips;
    bool update_screen;
    int vertical;
    int horizontal;
    TTF_Font *font;
    SDL_Color text_color;
    FullTexture device_text = {nullptr, {0, 0}};
    FullTexture freq_text = {nullptr, {0, 0}};
    FullTexture samp_rate_text = {nullptr, {0, 0}};
    bool device_connected;
    bool is_transmitting;
    size_t direction;
};


#endif //C_COMM_INTERFACE_H
