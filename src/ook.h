#ifndef HRF_OOK_H
#define HRF_OOK_H

#define ENABLED 1
#define DISABLED 0
#define false 0
#define true 1

#include <libhackrf/hackrf.h>
#include <stdio.h>
#include <stdlib.h>

typedef int16_t signal_pt;

const signal_pt max_amplitude = 32767;
const signal_pt offset_freq = 2500;

typedef struct {
    signal_pt *signal;
    unsigned long length;
} rf_signal;

typedef struct {
    uint32_t sample_rate;
    uint32_t gain;
    uint64_t base_frequency;
    uint8_t amp;
    unsigned int grid_us;
} ook_configs;

typedef struct {
    unsigned int *message;
    unsigned int length;
} msg_descr;

enum week {
    FWD = 0,
    BACK = 1,
    RIGHT = 2,
    LEFT = 3,
    FWDRIGH = 4,
    FWDLEFT = 5,
    BACKRIGHT = 6,
    BACKLEFT = 7,
    STOP = 8,
};

//ook_configs prompt_configs();
void exit_hackrf_error(enum hackrf_error);

void setup_device(hackrf_device **, ook_configs);

int tx_callback(hackrf_transfer *);

void killsig_handler(int);

rf_signal key_on_for(unsigned int duration_us, uint32_t sample_rate);

rf_signal key_off_for(unsigned int duration_us, uint32_t sample_rate);

unsigned long
message_duration_us(unsigned int *message, unsigned long length, unsigned long on_len, unsigned long off_len);

rf_signal encode_message(msg_descr message, rf_signal on, rf_signal off);

rf_signal repeat_encoded_message(rf_signal message, unsigned long duration_us, uint32_t sample_rate);

msg_descr generate_msg_descriptor(unsigned int long_pulses, unsigned int short_pulses);

#endif
