#include "CarController.h"
#include "Interface.h"
#include <chrono>
#include <unistd.h>
#include <iostream>

int main() {
    Interface interface= Interface();
    CarController controller(384, 27231000);
    interface.mainLoopStep(&CarController::startTX, &CarController::stopTX, &controller);
    controller.setupHackRF(&Interface::setHackRf, &interface);
    auto conn_attempt = std::chrono::system_clock::now();
    bool keep_running = true;

    while (keep_running) {
        keep_running = interface.mainLoopStep(&CarController::startTX, &CarController::stopTX, &controller);

        if(!controller.isConnected()) {
            auto now = std::chrono::system_clock::now();
            std::chrono::duration<double> diff = now - conn_attempt;

            controller.setupHackRF(&Interface::setHackRf, &interface);

            if(diff.count() < 1) {
                SDL_Delay(200);
                continue;
            }
            conn_attempt = std::chrono::system_clock::now();
        }

        usleep(200);
    }
    return 0;
}
