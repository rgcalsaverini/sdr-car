#include <string>
#include "Interface.h"
#include "CarController.h"
#include <iostream>

Interface::Interface() {
    char title[] = "RF Car Control";
    SDL_Init(SDL_INIT_VIDEO);
    window = SDL_CreateWindow(title, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 700, 500, SDL_WINDOW_SHOWN);
    screen = SDL_GetWindowSurface(window);
    IMG_Init(IMG_INIT_PNG);
    TTF_Init();
    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
    SDL_SetRenderDrawColor(renderer, 0xFF, 0xFF, 0xFF, 0xFF);

    sprites_img = loadTexture("./sprites.png");
    font = TTF_OpenFont("./berthold_akzidenz.ttf", 32);
    text_color = {72, 55, 55};
    setDeviceId();
    setFrequency();
    setSampleRate();

    sprite_clips.resize(SPRITES_LEN);

    /* Background */
    sprite_clips[BG] = (SDL_Rect) {.x = 0, .y = 0, .w = 700, .h = 500};
    sprite_clips[ARROW_T] = (SDL_Rect) {.x = 0, .y = 500, .w = 65, .h = 45};
    sprite_clips[ARROW_B] = (SDL_Rect) {.x = 65, .y = 500, .w = 65, .h = 45};
    sprite_clips[ARROW_R] = (SDL_Rect) {.x = 130, .y = 500, .w = 45, .h = 65};
    sprite_clips[ARROW_L] = (SDL_Rect) {.x = 175, .y = 500, .w = 45, .h = 65};
    sprite_clips[ICO_RADIO] = (SDL_Rect) {.x = 525, .y = 530, .w = 46, .h = 53};
    sprite_clips[CAR] = (SDL_Rect) {.x = 220, .y = 500, .w = 256, .h = 367};
    sprite_clips[TX_FALSE] = (SDL_Rect) {.x = 525, .y = 500, .w = 65, .h = 30};
    sprite_clips[TX_TRUE] = (SDL_Rect) {.x = 590, .y = 500, .w = 65, .h = 30};

    capture_keys[SDLK_w] = UP;
    capture_keys[SDLK_UP] = UP;
    capture_keys[SDLK_a] = LEFT;
    capture_keys[SDLK_LEFT] = LEFT;
    capture_keys[SDLK_s] = DOWN;
    capture_keys[SDLK_DOWN] = DOWN;
    capture_keys[SDLK_d] = RIGHT;
    capture_keys[SDLK_RIGHT] = RIGHT;

    key_down[UP] = false;

    key_down[DOWN] = false;
    key_down[RIGHT] = false;
    key_down[LEFT] = false;
    is_transmitting = false;

    update_screen = true;
    vertical = 0;
    horizontal = 0;
}

Interface::~Interface() {
    SDL_DestroyWindow(window);
    SDL_FreeSurface(screen);
    TTF_CloseFont(font);
    SDL_DestroyTexture(sprites_img);
    SDL_DestroyTexture(device_text.first);
    SDL_DestroyTexture(freq_text.first);
    SDL_DestroyTexture(samp_rate_text.first);
    SDL_DestroyRenderer(renderer);
    TTF_Quit();
    IMG_Quit();
    SDL_Quit();
}

SDL_Texture *Interface::loadTexture(std::string path) {
    SDL_Texture *texture;
    SDL_Surface *loaded_surface = IMG_Load(path.c_str());
    texture = SDL_CreateTextureFromSurface(renderer, loaded_surface);
    SDL_FreeSurface(loaded_surface);
    return texture;
}

void Interface::setDeviceId(std::string device) {
    if(device_text.first != nullptr) {
        SDL_DestroyTexture(device_text.first);
    }

    if (!device.empty()) {
        device_text = text2Texture(device);
        device_connected = true;
    } else {
        device_text = text2Texture("none");
        device_connected = false;
    }
    update_screen = true;
}

string Interface::long2HzString(unsigned long hz_val) {
    char freq_cstr[20];

    if (hz_val == 0) {
        sprintf(freq_cstr, "-");
    } else if (hz_val >= 1000000000) {
        sprintf(freq_cstr, "%.2f GHz", hz_val / 1000000000.0);
    } else if (hz_val >= 1000000) {
        sprintf(freq_cstr, "%.2f MHz", hz_val / 1000000.0);
    } else if (hz_val >= 1000) {
        sprintf(freq_cstr, "%.2f KHz", hz_val / 1000.0);
    } else {
        sprintf(freq_cstr, "%.2f Hz", (float) hz_val);
    }

    return string(freq_cstr);
}

void Interface::setFrequency(unsigned long frequency) {
    if(freq_text.first != nullptr) {
        SDL_DestroyTexture(freq_text.first);
    }
    freq_text = text2Texture(long2HzString(frequency));
    update_screen = true;
}

void Interface::setSampleRate(unsigned long sr) {
    if(samp_rate_text.first != nullptr) {
        SDL_DestroyTexture(samp_rate_text.first);
    }
    samp_rate_text = text2Texture(long2HzString(sr));
    update_screen = true;
}

void Interface::setHackRf(std::string device, unsigned long frequency, unsigned long sr) {
    setDeviceId(device);
    setFrequency(frequency);
    setSampleRate(sr);
}

Interface::FullTexture Interface::text2Texture(std::string text) {
    SDL_Surface *text_surf = TTF_RenderText_Blended(font, text.c_str(), text_color);
    SDL_Texture *texture = SDL_CreateTextureFromSurface(renderer, text_surf);
    SDL_FreeSurface(text_surf);
    return {texture, {text_surf->w, text_surf->h}};
}

void Interface::calcDirection() {
    if (vertical == 1) {
        if (horizontal == 1) {
            direction = CarController::Directions::FWDRIGHT;
        } else if (horizontal == 0) {
            direction = CarController::Directions::FWD;
        } else {
            direction = CarController::Directions::FWDLEFT;
        }
    } else if (vertical == 0) {
        if (horizontal == 1) {
            direction = CarController::Directions::RIGHT;
        } else if (horizontal == 0) {
            direction = CarController::Directions::STOP;
        } else {
            direction = CarController::Directions::LEFT;
        }
    } else {
        if (horizontal == 1) {
            direction = CarController::Directions::BACKRIGHT;
        } else if (horizontal == 0) {
            direction = CarController::Directions::BACK;
        } else {
            direction = CarController::Directions::BACKLEFT;
        }
    }
}

void Interface::renderScreen() {
    renderSprite(BG, 0, 0);
    renderCenteredText(device_text, 442, 385, 250);
    renderCenteredText(samp_rate_text, 442, 282, 250);
    renderCenteredText(freq_text, 442, 180, 250);

    if (device_connected) {
        renderSprite(ICO_RADIO, 390, 373);
        renderSprite(CAR, 60, 66);
        renderSprite(is_transmitting ? TX_TRUE : TX_FALSE, 57, 418);
    }

    if (key_down[UP]) {
        renderSprite(ARROW_T, 158, 112);
    }

    if (key_down[DOWN]) {
        renderSprite(ARROW_B, 158, 352);
    }

    if (key_down[RIGHT]) {
        renderSprite(ARROW_R, 247, 222);
    }

    if (key_down[LEFT]) {
        renderSprite(ARROW_L, 89, 222);
    }

    SDL_RenderPresent(renderer);
    update_screen = false;
}

bool
Interface::mainLoopStep(void(CarController::*startTX)(size_t), void(CarController::*stopTX)(), CarController *obj) {
    bool prev_transmitting = is_transmitting;
    size_t prev_direction = direction;
    if (!poolEvents()) {
        return false;
    }

    calcDirection();

    if (direction == CarController::Directions::STOP) {
        if (prev_transmitting) {
            (*obj.*stopTX)();
        }
    } else {
        if (prev_direction != direction) {
            (*obj.*startTX)(direction);
        }
    }

    if (update_screen) {
        renderScreen();
    } else {
        SDL_Delay(10);
    }

    return true;
}

void Interface::renderSprite(size_t sprite, int x, int y) {
    SDL_Rect dst_rect = (SDL_Rect) {.x = x, .y = y, .w = sprite_clips[sprite].w, .h = sprite_clips[sprite].h};
    SDL_RenderCopy(renderer, sprites_img, &sprite_clips[sprite], &dst_rect);
}

void Interface::renderCenteredText(Interface::FullTexture texture, int x, int y, int w) {
    SDL_Rect dst_rect = (SDL_Rect) {
            .x = x + (w - texture.second.first) / 2,
            .y = y,
            .w = texture.second.first,
            .h = texture.second.second
    };
    SDL_RenderCopy(renderer, texture.first, NULL, &dst_rect);
}

bool Interface::poolEvents() {
    SDL_Event event;

    while (SDL_PollEvent(&event) != 0) {
        if (event.type == SDL_QUIT || (event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_ESCAPE)) {
            return false;
        }

        if (!device_connected) {
            return true;
        }

        for (auto const &cap_key : capture_keys) {
            if (event.key.keysym.sym == cap_key.first) {
                if (event.type == SDL_KEYDOWN) {
                    update_screen = !key_down[cap_key.second];
                    key_down[cap_key.second] = true;
                } else if (event.type == SDL_KEYUP) {
                    update_screen = key_down[cap_key.second];
                    key_down[cap_key.second] = false;
                }
            }
        }

    }

    vertical = key_down[UP] - key_down[DOWN];
    horizontal = key_down[RIGHT] - key_down[LEFT];
    is_transmitting = vertical != 0 | horizontal != 0;

    return true;
}