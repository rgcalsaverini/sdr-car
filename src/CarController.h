//
// Created by rui on 20.04.18.
//

#ifndef C_COMM_CARCONTROLLER_H
#define C_COMM_CARCONTROLLER_H

#define SP_RTE 2000000
#define GAIN 47
//#define MAX_AMPLITUDE 32767.0;
#define OFFSET_FREQ 2000;

#include <libhackrf/hackrf.h>
#include <utility>
#include <vector>
#include <string>
#include <map>
#include <functional>
#include "Interface.h"

using std::vector;
using std::map;
using std::pair;

class CarController {
public:
    enum Directions : size_t {
        FWD, BACK, RIGHT, LEFT, FWDRIGHT, FWDLEFT, BACKRIGHT, BACKLEFT, STOP
    };
//    using MsgDescriptor = pair<pair<unsigned int, unsigned int>, unsigned int>; // (
    using signal_t = uint8_t;
    using SignalU8 = pair<signal_t*, size_t>;
public:
    CarController(unsigned int grid_us, uint64_t base_frequency, uint32_t sample_rate = SP_RTE, uint32_t gain = GAIN);

    ~CarController();

    bool setupHackRF(void(Interface::*onReady)(std::string, unsigned long, unsigned long), Interface *obj);

    bool isConnected() {return device != NULL && device != nullptr;};

    void startTX(size_t direction);

    void stopTX();

private:

    static int TXCallback(hackrf_transfer *transfer);

    SignalU8 keyOnFor(unsigned int duration_us);

    SignalU8 keyOffFor(unsigned int duration_us);

    SignalU8 encodeMessage(unsigned int short_pulses);


private:
    hackrf_device *device = nullptr;
    uint32_t sample_rate;
    uint32_t gain;
    uint64_t base_frequency;
    bool amp;
    unsigned int grid_us;
    map<size_t, unsigned int> msg_descriptors;
    SignalU8 key_on;
    SignalU8 key_off;
    unsigned long grid_samples;
    read_partid_serialno_t part_serial_number;

};

#endif //C_COMM_CARCONTROLLER_H
