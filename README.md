# SDR Car

This is a personal introduction to radio security.
 
This project consists on a simple UI that allows the user to highjack and control RC toy cars using
an SDR (software defined radio) device. 

![](./doc/image.jpg)

This is currently very, very buggy :) The new version is not working properly, but the previous one, (despite some modulation
issues) works well enough. To build it, just runt `make old`

There are some dependencies:

```shell script
sudo apt install 
```